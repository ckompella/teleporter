package com.company.teleport;

public class Main {

    // for the sake of simplicity, the input is taken in as a list of strings
    // we can use a File reader to get this input or as needed per requirements
    private final static String[] testInput = {
            "Fortuna - Hemingway",
            "Fortuna - Atlantis",
            "Hemingway - Chesterfield",
            "Chesterfield - Springton",
            "Los Amigos - Paristown",
            "Paristown - Oaktown",
            "Los Amigos - Oaktown",
            "Summerton - Springton",
            "Summerton - Hemingway",
            "cities from Summerton in 1 jumps",
            "cities from Summerton in 2 jumps",
            "can I teleport from Springton to Atlantis",
            "can I teleport from Oaktown to Atlantis",
            "loop possible from Oaktown",
            "loop possible from Fortuna"
    };

    public static void main(String[] args) {
        teleportTest();
    }

    private static void teleportTest() {

        CommandProcessor commandProcessor = new CommandProcessor();

        for (String s : testInput) {
            if (s.startsWith("cities")) {
                commandProcessor.processCitiesWithNJumps(s);
            } else if (s.startsWith("loop")) {
                commandProcessor.processLoopCheck(s);
            } else if (s.startsWith("can")) {
                commandProcessor.processTeleportCheck(s);
            } else {
                commandProcessor.processRoute(s);
            }
        }
    }
}
