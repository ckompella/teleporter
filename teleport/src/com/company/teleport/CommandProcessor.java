package com.company.teleport;

import com.sun.istack.internal.NotNull;

import java.util.Scanner;
import java.util.Set;
import java.util.regex.MatchResult;

public class CommandProcessor {

    private final TeleportGraph graph = new TeleportGraph();

    /**
     * adds the nodes to Graph and updates adjacencies
     *
     * @param input route should be in format : "Fortuna - Hemingway"
     */
    public void processRoute(@NotNull String input) {
        try (Scanner s = new Scanner(input)) {
            s.findInLine("(\\w+) - (\\w+)");
            MatchResult result = s.match();
            if (result.groupCount() != 2) {
                throw new IllegalArgumentException(input + " is not in expected for parsing route");
            }
            String city1 = result.group(1);
            String city2 = result.group(2);

            if (city1.equals(city2)) {
                System.out.println("City name is duplicated");
            }

            graph.addAdjacent(city1, city2);
        } catch (Exception e) {
            System.out.println("Invalid input to parse route : " + input);
        }
    }

    /**
     * Checks if one can teleport from City1 to City2 with input
     *
     * @param input - query to be in the format: "can I teleport from Springton to Atlantis"
     */
    public void processTeleportCheck(@NotNull String input) {

        try (Scanner s = new Scanner(input)) {
            s.findInLine("can I teleport from (\\w+) to (\\w+)");
            MatchResult result = s.match();
            if (result.groupCount() != 2) {
                throw new IllegalArgumentException("invalid input");
            }
            String city1 = result.group(1);
            String city2 = result.group(2);
            boolean status = graph.canTeleport(city1, city2);
            System.out.println("can I teleport from " + city1 + " to " + city2 + ": " + (status ? "yes" : "no"));
        } catch (Exception e) {
            System.out.println("Invalid input to parse TeleportCheck :" + input);
        }
    }

    /**
     * Checks if there is a loop from a given city
     *
     * @param input : expected format "loop possible from CityName"
     */
    public void processLoopCheck(@NotNull String input) {

        try (Scanner s = new Scanner(input).useDelimiter("loop possible from\\s*")) {
            String city = s.next();
            boolean hasCycle = graph.hasCycleFromCity(city);
            System.out.println("loop possible from " + city + ": " + (hasCycle ? "yes" : "no"));
        } catch (Exception e) {
            System.out.println("Invalid input for Loop Check : " + input);
        }
    }

    /**
     * Gets the list of cities that can be reached in the graph within N jumps
     *
     * @param input - expected format : "cities from Summerton in 1 jumps"
     */
    public void processCitiesWithNJumps(@NotNull String input) {

        try (Scanner s = new Scanner(input)) {
            s.findInLine("cities from (\\w+) in (\\d+) jumps");
            MatchResult result = s.match();
            if (result.groupCount() != 2) {
                throw new IllegalArgumentException("invalid input");
            }
            String city = result.group(1);
            int jumps = Integer.parseInt(result.group(2));

            Set<String> set = graph.getCitiesWithNJumps(city, jumps);
            set.remove(city);
            String citiesStr = set.toString().replace("[", "").replace("]", "");
            System.out.println("cities from " + city + " in " + jumps + " jumps: " + citiesStr);
        } catch (Exception e) {
            System.out.println("Invalid input for finding Cities within jumps : " + input);
        }
    }
}
