package com.company.teleport;

import com.sun.istack.internal.NotNull;

import java.util.*;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * Simple Implementation of Undirected graph for Teleport problem
 */
class TeleportGraph {
    private final HashMap<String, TeleportNode> nodes = new HashMap();

    /**
     * Adds cities as adjacent to each other
     * @param city1
     * @param city2
     * @throws IllegalArgumentException if either of the cities is NULL
     */
    public void addAdjacent(String city1, String city2) throws IllegalArgumentException {
        TeleportNode city1Node;
        TeleportNode city2Node;

        if (city1 == null || city2 == null) throw new IllegalArgumentException("Invalid citynames");

        if (!nodes.containsKey(city1)) {
            city1Node = new TeleportNode(city1);
            nodes.put(city1, city1Node);
        } else {
            city1Node = nodes.get(city1);
        }

        if (!nodes.containsKey(city2)) {
            city2Node = new TeleportNode(city2);
            nodes.put(city2, city2Node);
        } else {
            city2Node = nodes.get(city2);
        }

        // add adjacency both ways
        city1Node.getAdjacencyList().add(city2Node);
        city2Node.getAdjacencyList().add(city1Node);
    }

    /**
     * gets the Set of cities that are reachable from given cityName within given number of jumps
     * @param cityName
     * @param jumps number of jumps from given city
     * @return Set of cities that are reachable from given cityName within given number of jumps
     */
    public Set<String> getCitiesWithNJumps(@NotNull String cityName, int jumps) {
        Set<String> set = citiesFromCityHelper(cityName, jumps, new HashSet<>());  // handles jumps < 0
        set.remove(cityName);
        return set;
    }

    /**
     * Helper method to find cities with a set of cities that are already visited
     * @param city name of the city
     * @param jumps number of jumps from given city
     * @param visited set of cities already visited before this is called
     * @return
     */
    private Set<String> citiesFromCityHelper(@NotNull String city, int jumps, Set<String> visited) {
        if (jumps < 1) {
            return visited;
        }

        if (nodes.containsKey(city)) {
            TeleportNode g = nodes.get(city);
            List<TeleportNode> list = g.getAdjacencyList();
            for (TeleportNode node : list) {
                if (!visited.contains(node.getCityName())) {
                    visited.add(node.getCityName());
                    visited.addAll(citiesFromCityHelper(node.getCityName(), jumps - 1, visited));
                }
            }
        }

        return visited;
    }

    /**
     * checks if there is a connection between two given cities
     * @param cityName1
     * @param cityName2
     * @return true if connected
     */
    public boolean canTeleport(@NotNull String cityName1, @NotNull String cityName2) {

        HashSet<String> visited = new HashSet<>();

        if (!nodes.containsKey(cityName1) || !nodes.containsKey(cityName2)) {
            return false;
        }

        if (cityName1.equalsIgnoreCase(cityName2)) {
            return true;
        }

        TeleportNode n1 = nodes.get(cityName1);

        Queue<TeleportNode> queue = new LinkedBlockingDeque<>();
        queue.addAll(n1.getAdjacencyList());
        while (!queue.isEmpty()) {
            TeleportNode node = queue.remove();
            if (visited.contains(node.getCityName())) {
                continue;
            }
            if (node.getCityName().equals(cityName2)) {
                return true;
            }
            visited.add(node.getCityName());
            queue.addAll(node.getAdjacencyList());
        }
        return false;
    }

    /**
     * Method to check if there is a cycle from a given City name
     * @param cityName
     * @return true if there is a cycle and false otherwise
     */
    public boolean hasCycleFromCity(@NotNull String cityName) {
        TeleportNode node = nodes.get(cityName);
        HashSet visitedSet = new HashSet();

        return isCycleHelper(node, visitedSet, null);
    }

    /**
     *
     * @param currentNode - check cycle from this node
     * @param visited set of cities already visited so far
     * @param parent - the caller node
     * @return if there is cycle from this node
     */
    private boolean isCycleHelper(@NotNull TeleportNode currentNode, @NotNull Set<TeleportNode> visited, TeleportNode parent) {
        if (currentNode != null) {
            visited.add(currentNode);
            if (currentNode.getAdjacencyList() != null) {
                for (int i = 0; i < currentNode.getAdjacencyList().size(); i++) {
                    TeleportNode g = currentNode.getAdjacencyList().get(i);
                    if (g != parent) {
                        if (visited.contains(g)) {
                            return true;
                        } else {
                            isCycleHelper(g, visited, currentNode);
                        }
                    }
                }
            }
        }
        return false;
    }
}