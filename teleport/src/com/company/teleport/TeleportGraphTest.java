package com.company.teleport;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;

public class TeleportGraphTest {
    private TeleportGraph graph;

    @Before
    public void setUp() throws Exception {
        graph = new TeleportGraph();
    }

    @After
    public void tearDown() throws Exception {
    }

    @org.junit.Test(expected = IllegalArgumentException.class)
    public void addAdjacentVerifyException() {
        graph.addAdjacent(null, null);
    }

    @org.junit.Test
    public void getCitiesWithNJumps() {
        graph.addAdjacent("A", "B");
        Assert.assertEquals(0, graph.getCitiesWithNJumps("A", -1).size());
        Assert.assertEquals(0, graph.getCitiesWithNJumps("A", 0).size());
        Assert.assertEquals(1, graph.getCitiesWithNJumps("A", 2).size());

        graph.addAdjacent("C", "D");
        Assert.assertEquals(1, graph.getCitiesWithNJumps("A", 2).size());

        graph.addAdjacent("B", "C");

        Assert.assertEquals(1, graph.getCitiesWithNJumps("A", 1).size());
        Assert.assertEquals(2, graph.getCitiesWithNJumps("A", 2).size());
        Assert.assertEquals(3, graph.getCitiesWithNJumps("A", 3).size());
    }

    @org.junit.Test
    public void canTeleport() {

        graph.addAdjacent("A", "B");
        Assert.assertEquals(true, graph.canTeleport("A", "B"));
        Assert.assertEquals(false, graph.canTeleport("A", "C"));

        graph.addAdjacent("A", "C");
        Assert.assertEquals(true, graph.canTeleport("A", "C"));

        graph.addAdjacent("E", "F");
        Assert.assertEquals(false, graph.canTeleport("A", "F"));
    }

    @org.junit.Test
    public void hasCycleFromCity() {
        graph.addAdjacent("A", "B");
        graph.addAdjacent("C", "B");
        graph.addAdjacent("D", "C");
        graph.addAdjacent("A", "D");

        Assert.assertEquals(true, graph.hasCycleFromCity("A"));
        Assert.assertEquals(false, graph.hasCycleFromCity("..."));
    }
}