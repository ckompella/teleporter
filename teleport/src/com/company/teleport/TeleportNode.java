package com.company.teleport;

import java.util.ArrayList;
import java.util.List;

class TeleportNode {
    private final List<TeleportNode> adjacencyList = new ArrayList();
    private String cityName;

    private TeleportNode() {
    }

    public TeleportNode(String _cityName) {
        this.cityName = _cityName;
    }

    public List<TeleportNode> getAdjacencyList() {
        return adjacencyList;
    }

    public String getCityName() {
        return cityName;
    }
}
